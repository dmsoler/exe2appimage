#!/bin/bash
if [ -d "pkg2appimage" ] 
then
    cd pkg2appimage
    git pull
    cd ..
else
    git clone https://github.com/AppImage/pkg2appimage.git
fi
./pkg2appimage/pkg2appimage ./intef-exe.yml
mv ./out/* ./
rm -r out
./eXe*AppImage

