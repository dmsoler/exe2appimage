#!/bin/bash
if [ -d "pkg2appimage" ] 
then
    cd pkg2appimage
    git pull
    cd ..
else
    git clone https://github.com/AppImage/pkg2appimage.git
fi
if [ -d "iteexe" ] 
then
    cd iteexe
    git pull
    cd ..
else
    git clone https://github.com/exelearning/iteexe.git
fi
cd iteexe
echo "Branches in eXe's repository:"
git branch -a
echo "Please, type the name of eXe's branch to compile:"
read BRANCH
git stash
git checkout $BRANCH
git pull
echo "Now, you can modify the sources in ./iteexe if you want."
read -n 1 -s -r -p $'Press any key to compile when you are ready.\n'
cd installs/debian/ubuntu
python2 make.py
mkdir /tmp/debs
cp intef-exe*deb /tmp/debs/
cd ../../../../
./pkg2appimage/pkg2appimage ./intef-exe.yml
rm -r out
rm -rv intef-exe/intef-exe*deb
cp intef-exe/*deb /tmp/debs/
rm -rv intef-exe
./pkg2appimage/pkg2appimage ./intef-exe-src.yml
mv ./out/* ./
rm -r out
./eXe*AppImage

