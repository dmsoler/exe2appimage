# eXe2appimage
----
`eXe2appimage` is a collection of recipes and scripts to build [eXe learning](https://exelearning.net/en/) as an [AppImage](https://appimage.org).

`eXe2appimage` is free software under a GPL3 license (see [License](#license)).

# Contents
----
   * [Rationale](#rationale)
   * [How to use](#how-to-use)
      * [Step by step](#step-by-step) (English)
      * [Paso a paso](#paso-a-paso) (Spanish)
   * [Status](#status)
   * [AppImage documentation](#appimage-documentation)
   * [Author](#author)
   * [License](#license)

## Rationale

[eXe learning](https://exelearning.net/en/) should be a Python 3 application, and it is not (see [issue 95](https://github.com/exelearning/iteexe/issues/95) in its [GitHub repository](https://github.com/exelearning/iteexe/)). Since Python 2.7 is no longer supported, [eXe learning](https://exelearning.net/en/) cannot be installed or used in many recent Linux distributions (see thread [Linux install fails](https://exelearning.net/en/forums/topic/linux-install-fail/) in developers forum).

An AppImage could be a temporary solution to distribute a working bundle with all the Python 2.7 dependencies in place.

## How to use

Run the bash script [`eXedeb2appimage.sh`](eXedeb2appimage.sh): it will download [pkg2appimage](https://github.com/AppImage/pkg2appimage) and eXe's `.deb` package and build an AppImage. At the end of the process, you will find the resulting AppImage in the same directory.

If you want to build eXe's `.deb` package from last source in [its GitHub repository](https://github.com/exelearning/iteexe) before creating the AppImage, run the bash script [`eXesrc2appimage.sh`](eXesrc2appimage.sh) instead.

### Step by step

1. Clone this repository:

    ```bash
    git clone https://gitlab.com/dmsoler/exe2appimage.git
    ```
   
2. You will have a new directory `exe2appimage`. Move to it:

    ```bash
    cd exe2appimage
    ```
   
3. Run the script `eXedeb2appimage.sh`:

    ```bash
    ./eXedeb2appimage.sh
    ```
    
    If you prefer to build eXe's `.deb` package from last source in [its GitHub repository](https://github.com/exelearning/iteexe) before creating the AppImage, run the bash script `eXesrc2appimage.sh` instead:
    
    ```bash
    ./eXesrc2appimage.sh
    ```
   
    If everything goes well, at the end of the process you will have a new AppImage in this subdirectory, and you'll also see `eXe` opening in your default browser.
    
If you want to integrate the AppImage in your desktop, you may use [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher/). You'll find there [release packages for different Linux distros](https://github.com/TheAssassin/AppImageLauncher/releases) (see [this](https://github.com/TheAssassin/AppImageLauncher/#system-wide-installation) to choose the correct one), a [PPA for Ubuntu](https://launchpad.net/~appimagelauncher-team/+archive/ubuntu/stable) and even a [video tutorial](https://invidio.us/watch?v=D2WA2zdLvVk). After installing `AppImageLauncher` run again `eXe*AppImage` file and you'll see `AppImageLauncher` dialog.

You can also try the experimental and more recent [`appimaged`](https://github.com/probonopd/go-appimage), written in Go (see the [usage instructions](https://github.com/probonopd/go-appimage/blob/master/src/appimaged/README.md)).

### Paso a paso

1. Clonar este repositorio:

    ```bash
    git clone https://gitlab.com/dmsoler/exe2appimage.git
    ```
   
2. El paso anterior crea el directorio `exe2appimage`. Nos cambiamos a él:

    ```bash
    cd exe2appimage
    ```
   
3. Ejecutar el script `eXedeb2appimage.sh`:

    ```bash
    ./eXedeb2appimage.sh
    ```
    Si prefiere compilar el paquete `.deb` de eXe a partir de las últimas fuentes disponibles en [su repositorio en GitHub](https://github.com/exelearning/iteexe) antes de crear la AppImage, ejecute el script `eXesrc2appimage.sh` en vez del anterior:
    
    ```bash
    ./eXesrc2appimage.sh
    ``` 
   
    Si todo va bien, al final del proceso encontrará una nueva AppImage en este directorio, que será ejecutada automáticamente y verá cómo `eXe` se abre en su navegador por defecto.
    
Para integrar la AppImage en su entorno de escritorio, puede usar [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher/). Hay publicados [paquetes para diferentes distribuciones de Linux](https://github.com/TheAssassin/AppImageLauncher/releases) (consulte [esta información](https://github.com/TheAssassin/AppImageLauncher/#system-wide-installation) para escoger el correcto), un [PPA para Ubuntu](https://launchpad.net/~appimagelauncher-team/+archive/ubuntu/stable) y un [videotutorial](https://invidio.us/watch?v=D2WA2zdLvVk). Después de instalar `AppImageLauncher`, ejecute de nuevo el fichero `eXe*AppImage` y verá abrirse el cuadro de diálogo de `AppImageLauncher`.

Puede probar también el más reciente y experimental [`appimaged`](https://github.com/probonopd/go-appimage), escrito en Go (vea las [instrucciones de uso](https://github.com/probonopd/go-appimage/blob/master/src/appimaged/README.md)).

## Status

* 26/04/2020: you can build a fully working AppImage with packages from Debian stable distribution using the script `eXedeb2appimage.sh` and the [YAML file](intef-exe.yml).
* 28/04/2020: you can access full filesystem of the host system in eXe's menu (see [issue #1](https://gitlab.com/dmsoler/exe2appimage/-/issues/1)).
* 29/04/2020: desktop integration using [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher/).
* 23/05/2020: the [YAML file](intef-exe.yml) is now [merged](https://github.com/AppImage/pkg2appimage/pull/423) in [pkg2appimage](https://github.com/AppImage/pkg2appimage/) bundle, see it [here](https://github.com/AppImage/pkg2appimage/blob/master/recipes/intef-exe.yml). The script `eXedeb2appimage.sh` now uses the YAML file included in pkg2appimage. 
* 17/8/2020: new script `eXesrc2appimage.sh` to build the AppImage from an eXe's `.deb` compiled from the sources in [its GitHub repository](https://github.com/exelearning/iteexe), instead of from last official release.
* 21/11/2022: The YAML file include in `pkg2appimage` doesn't work since 24/8/2020 (see [issue #6](https://gitlab.com/dmsoler/exe2appimage/-/issues/6) and `pkg2appimage`'s [issue 429](https://github.com/AppImageCommunity/pkg2appimage/issues/429) and [issue 435](https://github.com/AppImageCommunity/pkg2appimage/issues/435)). The YAML files in this repository now explicitly add three dependency packages (`python-cffi-backend`, `python-minimal` and `python-zope.interface`, see [commit e7b053d2](https://gitlab.com/dmsoler/exe2appimage/-/commit/e7b053d2f5f9bb78775bdb6f6bab51c1c5d834ae)) and the scripts use these YAML files instead.
* 22/05/2023: The YAML files in this repository now use the new `eXeLearning.desktop` filename introduced in [this commit](https://github.com/exelearning/iteexe/commit/91fa194efa37fcf4db332701fa15411a6d9be9c9). This document was updated to include references to the [newer and experimental implementation in Go of AppImage tools](https://github.com/probonopd/go-appimage).

## AppImage documentation

The following AppImage documentation may be useful:

   * [Bundling Python apps](https://github.com/AppImage/AppImageKit/wiki/Bundling-Python-apps)
   * [About `.yml` files used in pkg2appimage](https://github.com/AppImage/pkg2appimage/blob/master/YML.md)

## Author

* **Dionisio Martínez Soler**

## License

`eXe2appimage` is free software under a GNU General Public License version 3 or later. See [LICENSE](LICENSE) file for more information.

